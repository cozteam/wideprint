from django.contrib.auth.models import User
from django.db import models
from jsonfield import JSONField
from django.core.mail import send_mail
import secrets
import os
import random
import string


def randomString(stringLength=32):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))

class YandexGoogleCounters(models.Model):
    class Meta:
        verbose_name_plural = 'statistic counters'
    yandex_counter = models.CharField(max_length=1000)
    google_counter = models.CharField(max_length=1000)

TYPE_OF_CARD = (
('Banner', 'Баннер'), # Первое значение для БД, второе выводится на дисплей
('Vinyl', 'Пленка'),
('PVC', 'Печать на пластике')
)

class Products(models.Model):
    class Meta:
        verbose_name_plural = 'products'
    product_type = models.CharField(max_length=50, choices=TYPE_OF_CARD)

class BannerPriceProduct(models.Model):
    class Meta:
        verbose_name_plural = 'products price'
    print_prices = JSONField()
    rings = models.FloatField(max_length=10,blank=True, null=True)
    edges = models.FloatField(max_length=10,blank=True, null=True)
    option_3_pocket = models.FloatField(max_length=10,blank=True, null=True)
    option_4_cord = models.FloatField(max_length=10,blank=True, null=True)

    def __str__(self):
        return str('Banner')

## class Clients(models.Model):
#     class Meta:
#         verbose_name_plural = 'clients'

ORDER_STATUS = (
('pending', 'В ожидании'), # Первое значение для БД, второе выводится на дисплей
('printing', 'В печати'),
('complete', 'Готово')
)

HEAD_ORDER_STATUS = (
('no_pay', 'Заказ не оплачен'),
('not_ready', 'Заказ не готов'),
('ready', 'Заказ готов'), # Первое значение для БД, второе выводится на дисплей
)

PRODUCT_TYPE = (
('banner_440', 'Баннер ламинированный 440 гр./кв.м'), # Первое значение для БД, второе выводится на дисплей
('banner_500', 'Баннер ламинированный 500 гр./кв.м'),
('banner_510', 'Баннер литой 510 гр./кв.м.'),
('vinyl_orajet', 'Самоклющаяся пленка ORAJET, белая'),
('vinyl_china', 'Самоклющаяся пленка Китай, белая'),
('pvc_1', 'ПВХ 1 мм'),
('pvc_2', 'ПВХ 2 мм'),
('pvc_4', 'ПВХ 4 мм'),
('pvc_5', 'ПВХ 5 мм'),
('pvc_10', 'ПВХ 10 мм'),
)

class HeadOrders(models.Model):
    user = models.ForeignKey(
        User,
        on_delete=models.DO_NOTHING,
        verbose_name='Заказчик'
    )
    delivery = models.BooleanField(default=False)
    address = models.CharField(max_length=50, blank=True, null=True)
    comments = models.CharField(max_length=50, blank=True, null=True)
    total_price_of_orders = models.IntegerField(default=0)
    status = models.CharField(max_length=50, choices=HEAD_ORDER_STATUS, default=1)
    success_token = models.CharField(
        default=randomString,
        max_length=255
    )


    def save(self, *args, **kwargs):
        email = self.user.email
        self.total_price_of_orders = 1
        if self.status == 'ready':
            send_mail(
                'Ваш заказ готов!',
                'Ваш заказ готов!',
                'wideprintmoscow@yandex.ru',
                [email],
                fail_silently=False,
            )
        super().save(*args, **kwargs)

    def __str__(self):
        try:
            return self.user.username
        except BaseException:
            return str(self.id)

    class Meta:
        verbose_name_plural = 'HeadOrders'

class Orders(models.Model):
    product_type = models.CharField(max_length=50, choices=PRODUCT_TYPE)
    quantity = models.IntegerField(default=0)
    edges = models.BooleanField(default=False)
    rings = models.BooleanField(default=False)
    cord = models.BooleanField(default=False)
    pocket = models.BooleanField(default=False)
    dpi = models.CharField(max_length=10)
    design = models.BooleanField()
    description = models.CharField(max_length=500, blank=True, null=True)
    size_v = models.CharField(max_length=50, default=100) # remove blank and null in production
    size_h = models.CharField(max_length=50, default=100)
    file = models.FileField(
        max_length=255,
        default=None,
        blank=True,
        null=True,
        upload_to='orders/%Y/%m/%d',
    ) # MEDIA_ROOT/documents/
    total_price = models.DecimalField(default=0, max_digits=30, decimal_places=2)
    status = models.CharField(max_length=50, choices=ORDER_STATUS, default='pending')
    source = models.ForeignKey(HeadOrders, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return str(self.source)

    def save(self, *args, **kwargs):
        try:
            this_entry = Orders.objects.get(id=self.id)
            if this_entry.file != self.file:
                this_entry.file.delete(save=False)
        except:
            pass
        return super(Orders, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        self.file.delete(save=False)
        return super(Orders, self).delete(*args, **kwargs)

    def filename(self):
        return os.path.basename(self.file.name)

    class Meta:
        verbose_name_plural = 'Orders'

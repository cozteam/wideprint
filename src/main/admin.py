from django.contrib import admin
from main.models import BannerPriceProduct, Orders, YandexGoogleCounters, HeadOrders

admin.site.register(BannerPriceProduct)
admin.site.register(Orders)
admin.site.register(YandexGoogleCounters)
admin.site.register(HeadOrders)

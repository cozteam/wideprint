from django import forms
from main.models import Products, Orders, HeadOrders
from django.forms import widgets

# class BuyerForm(forms.ModelForm): # унаследуется от модели
#     class Meta:
#         model = Buyer # определяем, какая модель будет использоваться для создания формы
#         fields = ('instance_id',)

class ProductChoices(forms.ModelForm):
    class Meta:
        model = Products
        fields = ('product_type',)

class OrderForm(forms.ModelForm):
    class Meta:
        model = Orders
        fields = (
            'product_type',
            'quantity',
            'edges',
            'rings',
            'design',
            'size_v',
            'size_h',
            'description',
            'total_price',
        )

# , 'file', 'total_price'

class HeadOrderForm(forms.ModelForm):
    class Meta:
        model = HeadOrders
        fields = (
            'user',
            'delivery',
            'address',
            'comments',
            'total_price_of_orders',
        )

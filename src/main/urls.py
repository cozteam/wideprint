from django.urls import path
from . import views

app_name = 'main'
urlpatterns = [
    path('', views.basic, name='basic'),
    path('calculate_price/', views.calculate_price, name='calculate_price'),
    path('add_order_element/', views.add_order_element, name='add_order_element'),
    path('add_book_element/', views.add_book_element, name='add_book_element'),
    # path('submit_form/', views.submit_form, name='submit_form'),
    path('submit_head_form/', views.submit_head_form, name='submit_head_form'),
    path('verify_upload_file/', views.verify_upload_file, name='verify_upload_file'),

]

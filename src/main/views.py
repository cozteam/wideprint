import re
from django.core.exceptions import ObjectDoesNotExist
from django.core.files.uploadedfile import TemporaryUploadedFile, InMemoryUploadedFile
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render
import requests
import json
from django.contrib import auth
from django.contrib.auth.models import AnonymousUser, User
from django.urls import reverse
from main.forms import ProductChoices, OrderForm, HeadOrderForm
from main.models import BannerPriceProduct, YandexGoogleCounters, HeadOrders
from common_function.common_function import verification_password, verify_file_size
from common_function.mailer import send_mail


def basic(request):
    username = auth.get_user(request).username
    user = auth.get_user(request)
    anonym = AnonymousUser()
    # form = ProductChoices()
    if user == anonym:
        return render(request, 'main/order_element.html')
    else:
        return render(request, 'main/order_element.html', {'user':user})

def add_order_element(request):
    number_of_elements = request.GET.get('number_of_elements')
    number_of_elements = str(int(number_of_elements)+1)
    return render(
        request, 'main/order_element_copy.html',
        {
            'element_number': number_of_elements,
            'size_format': 'size_format_{}'.format(number_of_elements),
            'edges': 'edges_{}'.format(number_of_elements),
            'rings': 'rings_{}'.format(number_of_elements),
            'size_h': 'size_h_{}'.format(number_of_elements),
            'size_v': 'size_v_{}'.format(number_of_elements),
        }
    )

def add_book_element(request):
    return render(request, 'main/book.html', {'condition': 'hidden'})

def calculate_price(request):
    count = request.GET.get('quantity')
    product_type = request.GET.get('product_type')
    edges = request.GET.get('edges')
    rings = request.GET.get('rings')
    option_3_pocket = request.GET.get('pocket')
    option_4_cord = request.GET.get('cord')
    design = request.GET.get('design')
    dpi = request.GET.get('dpi')
    x = request.GET.get('size_v')
    y = request.GET.get('size_h')
    if not x:
        x = 100
    if not y:
        y = 100
    square_one = (float(x)*float(y))/10000
    square_total = (float(x)*float(y)*float(count))/10000
    # count perimeter
    perimeter = 2*(float(x)+float(y))*float(count)
    dpi = str(dpi.split(" ")[0])  # эта строчка
    prices = BannerPriceProduct.objects.values('print_prices')
    prices = str(prices[0]['print_prices']).replace("\'", "\"")
    thresholds = json.loads(prices)[product_type][dpi].keys() # dpi тут
    try:
        tt = min([int(i) for i in thresholds if square_total < int(i)])
    except Exception:
        tt = max(map(int, thresholds))
    price_per_meter = json.loads(prices)[product_type][dpi][str(tt)]
    print(price_per_meter)
    total_price = float(price_per_meter)*float(square_total)
    size = "{}x{}".format(x, y)

    if design == 'true':
        total_price += 1500
    if edges == 'true':
        total_price += perimeter*0.3
        edges_response = 'с проклейкой'
    else:
        edges_response = 'без проклейки'
    if rings == 'true':
        total_price += perimeter*0.3
        rings_response = 'с люверсами'
    else:
        rings_response = 'без люверсов'

    product_names = [
        ('banner_440', 'Баннер 440гр.'),
        ('banner_500', 'Баннер 500гр.'),
        ('banner_510', 'Баннер 510гр.'),
        ('vinyl_orajet', 'Пленка ORAJET'),
        ('vinyl_china', 'Пленка Китай'),
        ('pvc_2', 'Пластик ПВХ 2мм'),
        ('pvc_4', 'Пластик ПВХ 4мм'),
        ('pvc_5', 'Пластик ПВХ 5мм'),
        ('pvc_8', 'Пластик ПВХ 8мм'),
        ('pvc_10', 'Пластик ПВХ 10мм')
    ]

    for i in product_names:
        if product_type == i[0]:
            type_media_word = i[1]
            break
    total_price = round(total_price, 2)
    response_data = [str(entry) for entry in [total_price, price_per_meter, square_one, size, count, square_total,
                                              edges_response, rings_response, type_media_word, dpi]]
    return HttpResponse('+'.join(response_data))

def submit_head_form(request):
    def create_new_orders(client_data):
        client_data = json.loads(client_data)
        new_head_orders = HeadOrderForm(client_data)
        if new_head_orders.is_valid():
            new_head_orders = new_head_orders.save()
            orders_count = (request.POST.get('orders_count', ''))
            if orders_count.isdigit():
                orders_count = int(orders_count)
            for i in range(orders_count):
                order = request.POST.get(str(i), False)
                if order:
                    order = json.loads(order)
                    new_order = OrderForm(order)
                    new_order.instance.source = HeadOrders.objects.get(id=new_head_orders.id)
                    new_order.instance.dpi = order['dpi']
                    new_order.instance.pocket = order['pocket']
                    new_order.instance.cord = order['cord']
                    if new_order.is_valid():
                        print('order is valid')
                        new_order.save(commit=True)
                        order_file = request.FILES.get(''.join(['file', str(i)]), False)
                        if order_file:
                            new_order.instance.file = order_file
                            new_order.instance.save()
                            #Orders.objects.get(id=1).file.path
                            #verify_file_size(new_order.instance.file.path)
                    else:
                        print(new_order.errors)
            email = client_data.get('email', False)
            if not email:
                email = request.user.email
                response_data = {
                'result': 'OK',
                'head_order_id': new_head_orders.id,
                'price': new_head_orders.total_price_of_orders,
                }
            return JsonResponse({'result': 'OK', 'head_order_id': new_head_orders.id, })
        else:
            return JsonResponse({'result': 'ERROR'})

    if request.method == 'POST':
        print('='*90)
        try:
            if request.user.is_authenticated:
                return create_new_orders(request.POST.get('client_data', ''))
            else:
                client_data = json.loads(request.POST.get('client_data', ''))
                firstname_and_lastname = client_data.get('fullname', '')
                email = client_data.get('email', '')
                phone = client_data.get('phone', '')
                password = verification_password('')
                sign_up_url = reverse('authorization:signup')
                url = request.build_absolute_uri(reverse('main:basic'))
                session = requests.session()
                response = session.get(url)
                csrf = re.findall(r'name="csrfmiddlewaretoken" value="([a-zA-Z0-9]+)"', response.content.decode())[0]
                try:
                    User.objects.get(email=email)
                    return JsonResponse({'result': 'USER_EXIST'})
                except ObjectDoesNotExist:
                    response = session.post(request.build_absolute_uri(sign_up_url), data={
                        'csrfmiddlewaretoken': csrf,
                        'username': email,
                        'name': firstname_and_lastname,
                        'password': password,
                        'phone': phone
                    })
                    if response.status_code == 200:
                        try:
                            user = User.objects.get(email=email)
                            send_mail(
                                'Пароль для входа на сайт {}'.format(request.META.get('HTTP_HOST')),
                                'Ваш пароль для входа на сайт {} \n {}'.format(request.META.get('HTTP_HOST'), password),
                                email,
                            )
                            client_data = json.loads(request.POST.get('client_data', ''))
                            client_data['user'] = user.id
                            return create_new_orders(json.dumps(client_data))
                        except Exception as e:
                            print(e)
                            return JsonResponse({'result': 'ERROR'})
        except Exception as e:
            print(e)
            return JsonResponse({'result': 'ERROR'})


def verify_upload_file(request):
    print(request.POST)
    print(request.FILES)
    if request.method == 'POST':
        print('POST')
        file = request.FILES.get('file')
        if type(file) == TemporaryUploadedFile:
            print('TemporaryUploadedFile')
            if file:
                verify_response = verify_file_size(file.temporary_file_path())
                return JsonResponse(verify_response)
        elif type(file) == InMemoryUploadedFile:
            with open('/tmp/tmpuploadedfile', 'wb') as tmp:
                tmp.write(file.read())
                verify_response = verify_file_size('/tmp/tmpuploadedfile')
                return JsonResponse(verify_response)
    return JsonResponse({'result': 'ERROR'})

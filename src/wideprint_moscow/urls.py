"""wideprint_moscow URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import include
from django.conf import settings
from django.conf.urls.static import static
from django.views.generic.base import TemplateView

urlpatterns = [
    path('robots.txt', TemplateView.as_view(template_name='base/robots.txt',content_type='text/plain')),
    path('sitemap.xml', TemplateView.as_view(template_name='base/sitemap.xml',content_type='application/xml')),
    path('admin/', admin.site.urls),
    path('auth/', include('users_app.urls', namespace='authorization')),
    path('test_auth/', include('auth_.urls', namespace='test_authorization')),
    path('', include('main.urls', namespace='main')),
    path('order_status/', include('order_status.urls', namespace='order_status')),
    path('pay/', include('pay.urls', namespace='pay')),
    path('', include('single_pages.urls', namespace='single_pages')),
]+static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)\
+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

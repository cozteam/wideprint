import os
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.application import MIMEApplication
from .mail_requisites import mailbox_password, mailbox_login


def send_mail(subject, message_text, address, file=None):
    msg = MIMEMultipart()
    txt = MIMEText(message_text)
    msg.attach(txt)
    msg['Subject'] = f'{subject}'
    msg['From'] = mailbox_login
    msg['To'] = '{address}'.format(address=address)

    if file:
        with open(file, 'rb') as byte_file:
            file_to_attach = MIMEApplication(byte_file.read(), Name=os.path.basename(file))
        file_to_attach['Content-Disposition'] = 'attachment; filename="{}"'.format(os.path.basename(file))
        msg.attach(file_to_attach)

    s = smtplib.SMTP('smtp.yandex.ru', 587)
    s.ehlo()
    s.starttls()
    s.ehlo()
    s.login(mailbox_login, mailbox_password)
    s.sendmail(msg['From'], msg['To'], msg.as_string())




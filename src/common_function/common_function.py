import os
import random

from PIL import Image


def split_full_name(firstname_and_lastname):
    firstname_and_lastname = firstname_and_lastname.split(' ', 1)
    if len(firstname_and_lastname) == 1:
        firstname = firstname_and_lastname[0]
        lastname = ''
    elif len(firstname_and_lastname) == 2:
        firstname = firstname_and_lastname[0]
        lastname = firstname_and_lastname[1]
    else:
        firstname = ''
        lastname = ''
    return firstname, lastname


char_collection = [
            'QWERTYUIOPASDFGHJKLZXCVBNM',
            'qwertyuiopasdfghjklzxcvbnm',
            '1234567890',
            # '!@#$%^&*()'
        ]


def generate_password(char_count):
    def select_random_char(el):
        return el[random.randrange(0, len(el))]
    return ''.join(
        [
            select_random_char(char_collection[random.randrange(3)])
            for el in range(int(char_count))
        ]
    )


def verification_password(entered_password, char_count=16):
    def verify_part_of_collection(elemetnts):
        for char in elemetnts:
            if char in entered_password:
                return True
            else:
                continue
        return False
    if len(entered_password) >= char_count:
        for elemetnts in char_collection:
            if verify_part_of_collection(elemetnts):
                continue
            else:
                return verification_password(generate_password(char_count))
                # print('your password generate automatically. your password is: ' + passw_)
        #print(entered_password)
        return entered_password
    else:
        return verification_password(generate_password(char_count))


def verify_file_size(path_to_file, size_v=100, size_h=100):
    try:
        file = Image.open(path_to_file)
        h = file.size[0] / file.info.get('dpi', file.info.get('resolution'))[0]
        v = file.size[1] / file.info.get('dpi', file.info.get('resolution'))[0]
        os.remove(path_to_file)
        response = {
            'result': 'OK',
            'size_h': round(h),
            'size_v': round(v),
        }
        return response
    except Exception as e:
        response = {
            'result': 'ERROR',
            'exception': e,
            'size_h': '',
            'size_v': '',
        }
        return response

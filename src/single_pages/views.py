from django.shortcuts import render
from django.views.generic import View


class Requirements(View):
    """Requirements page"""
    def get(self, request):
        return render(request, 'requirements/requirements.html')

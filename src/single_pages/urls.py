# coding=utf-8
from django.urls import path
from . import views


app_name = 'single_pages'

urlpatterns = [
    path("order_requirements/", views.Requirements.as_view(), name="order_requirements"),
]

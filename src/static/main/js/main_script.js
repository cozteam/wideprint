$(document).ready(function () {
  $('body').on('click', '.popup__close', function () {
    $(this).parent().parent().fadeOut(300);
    setTimeout(function () {
      $('.upload_popup').removeClass('popup-shown');
    }, 300)
  })
  
  $('body').on('click', '#tab-pickup', function () {
    $('#address').prop('required',false).prop('disabled', true);
  })
  $('body').on('click', '#tab-delivery', function () {
    $('#address').prop('required',true).prop('disabled', false);
  })



});


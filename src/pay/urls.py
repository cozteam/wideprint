from django.urls import path
from . import views

app_name = 'pay'
urlpatterns = [
    path('yandex_redirect/<int:head_orders_id>/', views.yandex_redirect, name='yandex_redirect'),
    path('error/', views.PayError.as_view(), name='error'),
    path('success/<slug:token>/', views.pay_success, name='success'),

]

from django.shortcuts import render
from django.views.generic import TemplateView, View
from django.core.mail import EmailMessage
from django.contrib.auth.models import User
from django.urls import reverse
from main.models import HeadOrders

def yandex_redirect(request, **kwargs):
    if request.method == 'GET':
        order_id = kwargs.get('head_orders_id')
        order = HeadOrders.objects.get(id=order_id)
        context = {
            'order_id': order_id,
            'sum': order.total_price_of_orders,
            'success_token': order.success_token
        }
        return render(request, 'pay/yandex_redirect.html', context=context)


class PayError(TemplateView):
    template_name = "pay/pay_error.html"


def pay_success(request, **kwargs):
    if request.method == 'GET':
        token = kwargs.get('token')
        order = HeadOrders.objects.get(success_token=token)
        order.status = 'not_ready'
        user = User.objects.get(headorders__success_token=token)

        domain = request.build_absolute_uri('/')[:-1]
        unsubscribe_link = reverse('users_app:unsubscribe', kwargs={'token': user.userextend.unsubscribe_token})
        unsubscribe_url = '{domain}{path}'.format(domain=domain, path=unsubscribe_link)

        mail_for_send = EmailMessage(
            'Заказ успешно оплачен',
            body='Ваш заказ №{} принят в работу, ожидайте звонка оператора.'.format(order.id),
            from_email='wideprintmoscow@gmail.com',
            to=[user.email],
            headers={'Precedence': 'bulk', 'List-Unsubscribe': unsubscribe_url},

        )
        mail_for_send.send(fail_silently=False)
        order.save()
        return render(request, 'pay/pay_success.html')



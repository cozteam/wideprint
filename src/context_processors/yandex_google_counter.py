from main.models import YandexGoogleCounters


def counters(request):
    try:
        yandex_counter = YandexGoogleCounters.objects.all()[0].yandex_counter
        google_counter = YandexGoogleCounters.objects.all()[0].google_counter
    except:
        yandex_counter = ''
        google_counter = ''
    return {'yandex_counter': yandex_counter, 'google_counter': google_counter}

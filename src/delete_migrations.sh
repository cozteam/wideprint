find . -path "*/migrations/*.py" -not -name "__init__.py" -delete
find . -path "*/migrations/*.pyc"  -delete

echo "migrations removed"

# If ImportError: No module named 'django.db.migrations.migration'
# python -m django --version
# pip install --upgrade --force-reinstall  Django==2.0.5
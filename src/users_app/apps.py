from django.apps import AppConfig
from django.db.models.signals import post_save


class UserAppConfig(AppConfig):
    name = 'users_app'

    # def ready(self):
    #     from .signals import create_user_extend
    #     from django.contrib.auth import get_user_model
    #     User = get_user_model()
    #     post_save.connect(create_user_extend, sender=User)
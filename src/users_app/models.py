import uuid
from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
import secrets
from django.db.models.signals import post_save
from django.dispatch import receiver


class UserExtend(models.Model):
    user = models.OneToOneField(User,on_delete=models.CASCADE,verbose_name='Пользователь',)
    phone = models.CharField(default=0,null=True,blank=True,max_length=20)
    uuid = models.UUIDField(default=uuid.uuid4,editable=False,unique=True,verbose_name='unique user id',max_length = 255,)
    subscribed = models.BooleanField(default=True, max_length=255)
    unsubscribe_token = models.CharField(default=secrets.token_hex(nbytes=16),editable=False,verbose_name='token for unsubscribe user',max_length=255)

    def __str__(self):
        return self.user.username

    @receiver(post_save, sender=User)
    def create_user_profile(sender, instance, created, **kwargs):
        if created:
            UserExtend.objects.create(user=instance)

    @receiver(post_save, sender=User)
    def save_user_profile(sender, instance, **kwargs):
        instance.userextend.save()

    def get_activation_link(self):
        return reverse('users_app:activate', kwargs={'uuid': self.uuid})

    class Meta:
        verbose_name_plural = 'users'

from django.core.exceptions import ObjectDoesNotExist
from users_app.models import UserExtend


def create_user_extend(sender, instance, **kwargs):
    try:
        UserExtend.objects.get(user=instance)
    except ObjectDoesNotExist:
        user_extend = UserExtend()
        user_extend.user = instance
        user_extend.save()

from django.urls import path
from . import views

app_name = 'users_app'
urlpatterns = [
    path('login/', views.Login.as_view(), name='login'),
    path('logout/', views.logout_view, name='logout'),
    path('signup/', views.sign_up, name='signup'),
    path('activate/<str:uuid>/', views.activate, name='activate'),
    path('unsubscribe/<str:token>/', views.unsubscribe, name='unsubscribe'),

]

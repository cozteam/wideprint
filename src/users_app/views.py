from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from .models import UserExtend
from django.core.mail import send_mail
from django.core.mail import EmailMessage
from django.template import loader
from common_function.common_function import split_full_name
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from wideprint_moscow.settings import EMAIL_PORT, EMAIL_HOST, EMAIL_HOST_USER, EMAIL_HOST_PASSWORD
import smtplib

# Create your views here.
def logout_view(request):
    logout(request)

    return HttpResponseRedirect(reverse('users_app:login'))


class Login(LoginView):
    template_name = 'users_app/login_template.html'
    redirect_authenticated_user = '/'

    def post(self, request, *args, **kwargs):
        try:
            user = User.objects.get(username=request.POST.get('username', ''))
            if not user.is_active:
                return render(request, 'users_app/login_template.html', context={'activate': 'not_activate'})
            else:
                return super(Login, self).post(request, *args, **kwargs)
        except ObjectDoesNotExist:
            return render(request, 'users_app/login_template.html', context={'activate': 'wrong_user_data'})


def sign_up(request):
    form_data = request.POST
    firstname_and_lastname = form_data.get('name', '')
    email = form_data.get('username', False)
    phone = form_data.get('phone', '')
    password = form_data.get('password', False)
    if email and password:
        firstname, lastname = split_full_name(firstname_and_lastname)
        try:
            User.objects.get(username=email)
        except User.DoesNotExist:
            new_user = User()
            new_user.username = email
            new_user.email = email.lower()
            new_user.first_name = firstname
            new_user.last_name = lastname
            new_user.set_password(password)
            new_user.is_active = False
            new_user.save()
            activation_link = new_user.userextend.get_activation_link()
            domain = request.build_absolute_uri('/')[:-1]
            unsubscribe_link = reverse('users_app:unsubscribe', kwargs={'token': new_user.userextend.unsubscribe_token})
            unsubscribe_url = '{domain}{path}'.format(domain=domain, path=unsubscribe_link)

            html_message = loader.render_to_string(
                'mails/activate.html'
                ,
                {
                    'activation_link': request.build_absolute_uri(activation_link),
                    'unsubscribe': unsubscribe_url,
                    'email': new_user.email,
                }
            )

            mail_for_send = EmailMessage(
                    'Ссылка для активации аккаунта на сайте Широкоформатная-печать.москва',
                    body=html_message,
                    from_email=EMAIL_HOST_USER,
                    to=[new_user.email],
                    headers={'Precedence': 'bulk', 'List-Unsubscribe': unsubscribe_url},
                )
            mail_for_send.content_subtype = "html"
            mail_for_send.send(fail_silently=False)

    return render(request, template_name='users_app/login_template.html')


def activate(request, **kwargs):
    uuid = kwargs.get('uuid', None)
    if uuid:
        try:
            user_extend_obj = UserExtend.objects.get(uuid=uuid)
        except ObjectDoesNotExist:
            user_extend_obj = False

        if user_extend_obj and not user_extend_obj.user.is_active:
            user = User.objects.get(id=user_extend_obj.user.id)
            user.is_active = True
            user.save()
            user.backend = 'django.contrib.auth.backends.ModelBackend'
            login(request, user)

            return redirect('main:basic')
            # return render(request, template_name='users_app/login_template.html', context={'activate': 'success'})
        elif user_extend_obj.user.is_active:
            return render(request, template_name='users_app/login_template.html', context={'activate': 'early'})
    return render(request, template_name='users_app/login_template.html')


def unsubscribe(request, **kwargs):
    token = kwargs.get('token', None)
    if token:
        try:
            user = UserExtend.objects.get(unsubscribe_token=token)
        except ObjectDoesNotExist:
            return HttpResponse('Bad_user')
        user.subscribed = False
        user.save()
        return render(request, context={'user': user}, template_name='users_app/unsubscribe.html')
    return HttpResponse('No_token')








from django.template import loader
from django.core.mail import EmailMessage
from django.http import HttpResponse
import smtplib
import ssl
from wideprint_moscow.settings import EMAIL_PORT, EMAIL_HOST, EMAIL_HOST_USER, EMAIL_HOST_PASSWORD



def test(request):
    return HttpResponse("done")

def send_mail(request):

    html_message = loader.render_to_string('mails/activate.html',
        {'activation_link': 'aaa',
         'unsubscribe': 'aaa',
         'email': 'k.valov@mail.ru'})

    mail_for_send = EmailMessage(
        'Ссылка для активации аккаунта на сайте Широкоформатная-печать.москва',
        body=html_message,
        from_email='wideprintmoscow@gmail.com',
        to=['k.valov@mail.ru'],
        headers={'Precedence': 'bulk', 'List-Unsubscribe': 'aaa'},
    )
    mail_for_send.content_subtype = "html"
    mail_for_send.send(fail_silently=False)

    return HttpResponse("done")


def send_mail_ssl(request):
    port = EMAIL_PORT
    smtp_server = EMAIL_HOST
    sender_email = EMAIL_HOST_USER
    password = EMAIL_HOST_PASSWORD
    receiver_email = 'k.valov@mail.ru'
    subject = 'Website registration'
    body = 'Activate your account.'
    message = 'Subject: {}\n\n{}'.format(subject, body)
    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(smtp_server, port) as server:
        server.ehlo()  # Can be omitted
        server.starttls(context=context)
        server.ehlo()  # Can be omitted
        server.login(sender_email, password)
        server.sendmail(sender_email, receiver_email, message)
    return HttpResponse("done")

def send_mail_yandex(request):

    email = EMAIL_HOST_USER
    password = EMAIL_HOST_PASSWORD
    server = smtplib.SMTP(EMAIL_HOST, EMAIL_PORT)
    server.ehlo() # Кстати, зачем это?
    server.starttls()
    server.login(email, password)

    dest_email = 'k.valov@mail.ru'
    subject = 'Booking from chatbot'
    email_text = 'Text'
    message = 'From: %s\nTo: %s\nSubject: %s\n\n%s' % (email, dest_email, subject, email_text)

    server.set_debuglevel(1) # Необязательно; так будут отображаться данные с сервера в консоли
    server.sendmail(email, dest_email, message)
    server.quit()
    return HttpResponse("done")


def send_mail_yandex(request):
    email = EMAIL_HOST_USER
    password = EMAIL_HOST_PASSWORD
    server = smtplib.SMTP(EMAIL_HOST, EMAIL_PORT)
    server.ehlo()  # Кстати, зачем это?
    server.starttls()
    server.login(email, password)

    dest_email = [new_user.email]
    subject = 'Ссылка для активации аккаунта на сайте Широкоформатная-печать.москва'
    email_text = html_message
    message = 'From: %s\nTo: %s\nSubject: %s\n\n%s' % (email, dest_email, subject, email_text)

    server.set_debuglevel(1)  # Необязательно; так будут отображаться данные с сервера в консоли
    server.sendmail(email, dest_email, message)
    server.quit()
    return HttpResponse("done")

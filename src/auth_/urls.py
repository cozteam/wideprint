from django.urls import path
from . import views

app_name = 'auth_'
urlpatterns = [
    path('send_mail/', views.send_mail, name='send_mail'),
    path('send_mail_ssl/', views.send_mail_ssl, name='send_mail_ssl'),
    path('send_mail_test/', views.test, name='test'),
    path('send_mail_yandex/', views.send_mail_yandex, name='send_mail_yandex')
]

# path('login/', views.Login.as_view(), name='login'),
# path('logout/', views.logout_view, name='logout'),
# path('signup/', views.sign_up, name='signup'),
# path('activate/<str:uuid>/', views.activate, name='activate'),
# path('unsubscribe/<str:token>/', views.unsubscribe, name='unsubscribe'),
#!/usr/bin/env bash
cd /wide
echo "yes" | python manage.py collectstatic
gunicorn -w 3 --chdir ./ wideprint_moscow.wsgi --bind 0.0.0.0:8000

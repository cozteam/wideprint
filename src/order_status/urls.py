# coding=utf-8
from django.urls import path
from . import views


app_name = 'order_status'
urlpatterns = [
    path("", views.OrderStatus.as_view(), name='main'),
]

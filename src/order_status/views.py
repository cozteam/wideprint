from django.views.generic import View
from django.shortcuts import render
from main.models import Orders, HeadOrders


# Create your views here.


class OrderStatus(View):
    """Отображает страницу статуса заказа"""

    def get(self, request):
        if request.user.is_authenticated:
            user_orders = Orders.objects.filter(source__user__username=request.user.username)
            active_orders = user_orders.filter(status='pending')
            pending_orders = user_orders.filter(status='complete')
            total_price = 0
            for i in user_orders:
                total_price += i.total_price

            percent = (total_price / 440461) * 100
            if percent > 100:
                percent = 100
            print(total_price, percent)
            return render(request, 'order_status/status.html', {"active": active_orders,
                                                                "archive": pending_orders,
                                                                'percent': percent,
                                                                'total_price': total_price})
        else:
            return render(request, 'order_status/status.html')

#!/bin/bash

find . -name "*.pyc" -exec rm -f {} \;

echo "__pycache__ removed"

find . -name ".DS_Store" -exec rm -f {} \;

echo ".DS_Store removed"

import logging
from starlette.applications import Starlette
from starlette.background import BackgroundTask
from starlette.responses import Response
from starlette.routing import Route
import uvicorn
import git


LOG_FORMAT = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
logging.basicConfig(level=logging.DEBUG, format=LOG_FORMAT) # filename = u'mylog.log'
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)


async def webhook(request):
    LOGGER.debug('call "webhook" function')
    task = BackgroundTask(trigger_webhook)
    return Response("success", background=task)


async def trigger_webhook():
    LOGGER.debug('call "trigger_webhook" function')
    repo = git.Repo(".")
    g = git.cmd.Git(repo)
    result = g.pull("8055", "master")
    LOGGER.debug(result)
    return Response("success!!")


routes = [
Route("/webhook/bitbucket", endpoint=webhook, methods=["POST"]),
]

app = Starlette(routes=routes,)
